<?php
echo "<pre>";
print_r($_POST);

if (isset($_POST['btn'])) {
    $starting_number = $_POST['starting_number'];
    $ending_number = $_POST['ending_number'];
    $result='';
    if ($starting_number < $ending_number) {
        for ($i = $starting_number; $i <= $ending_number; $i++) {
            //echo $i.' ';
            $result.=$i.' ';
        }
    } else if ($starting_number > $ending_number) {
        for ($i = $starting_number; $i >= $ending_number; $i--) {
            //echo $i.' ';
            $result.=$i.' ';
        }
    }else{
        $result="Starting Number & Ending Number Must Not be Same";
        //echo $result;
    }
}
?>
<form  action="" method="POST">
    <table>
        <tr>
            <td>Starting Number</td>
            <td><input type="number" name="starting_number"></td>
        </tr>
        <tr>
            <td>Ending Number</td>
            <td><input type="number" name="ending_number"></td>
        </tr>
        <tr>
            <td>Result</td>
            <td><textarea cols="40" rows="10"><?php if(isset($result)){ echo $result;}?></textarea></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="btn" value="SUBMIT"></td>
        </tr>
    </table>
</form>
