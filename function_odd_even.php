<?php
echo '<pre>';
print_r($_POST);
//$res = '';
if (isset($_POST['btn'])) {

    function create_odd_even_series($data) {
        $res = '';
        $starting_number = $data['starting_number'];
        $ending_number = $data['ending_number'];

        if (isset($data['choice_btn'])) {
            $choice_btn = $data['choice_btn'];
            if ($starting_number > $ending_number) {
                if ($choice_btn == 'odd') {
                    for ($i = $starting_number; $i >= $ending_number; $i--) {
                        if ($i % 2 != 0) {
                            $res .= $i . ' ';
                        }
                    }
                } else {
                    for ($i = $starting_number; $i >= $ending_number; $i--) {
                        if ($i % 2 == 0) {
                            $res .= $i . ' ';
                        }
                    }
                }
            } else if ($starting_number < $ending_number) {
                if ($choice_btn == 'odd') {
                    for ($i = $starting_number; $i <= $ending_number; $i++) {
                        if ($i % 2 != 0) {
                            $res .= $i . ' ';
                        }
                    }
                } else {
                    for ($i = $starting_number; $i <= $ending_number; $i++) {
                        if ($i % 2 == 0) {
                            $res .= $i . ' ';
                        }
                    }
                }
            }
        } else {
            $res = "Please Select your Odd or Even";
        }
        return $res;
    }

    $res = create_odd_even_series($_POST);
}
?>
<form action="" method="post">
    <table>
        <tr>
            <td>Starting Number</td>
            <td><input type="number" name="starting_number"></td>
        </tr>
        <tr>
            <td>Ending Number</td>
            <td><input type="number" name="ending_number"></td>
        </tr>
        <tr>
            <td>My choice</td>
            <td>
                <input type="radio" name="choice_btn" value="odd">ODD
                <input type="radio" name="choice_btn" value="even">Even
            </td>
        </tr>
        <tr>
            <td>Result</td>
            <td>
                <textarea rows="8" cols="35"><?php echo $res; ?></textarea>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="btn" value="SUBMIT"></td>
        </tr>
    </table>
</form>

