<?php

function select_all_blog_info() {
    $db_connect = mysqli_connect('localhost', 'root', '');

    if ($db_connect) {
        mysqli_select_db($db_connect, 'db_seip_28');
    } else {
        die('Connect Failed' . mysqli_error($db_connect));
    }
    $sql = 'SELECT * FROM tbl_blog';
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

$query_result = select_all_blog_info();
?>
<hr/>
<a href="add_blog.php">ADD BLOG</a>
<a href="view_blog.php">View BLOG</a>
<hr/>
<table border="1" width='80%' cellpadding='5'>
    <tr>
        <th>Blog ID</th>
        <th>Blog Title</th>
        <th> Author Name</th>
        <th>Description</th>
        <th>Publication</th>
        <th>Action</th>

    </tr>
    <?php while ($blog_info = mysqli_fetch_array($query_result)) { ?>
        <tr>
            <td><?php echo $blog_info['blog_id']; ?></td>
            <td><?php echo $blog_info['blog_title']; ?></td>
            <td><?php echo $blog_info['author_name']; ?></td>
            <td><?php echo $blog_info['blog_description']; ?></td>
            <td>
                <?php
                if ($blog_info['publication_status'] == 1) {
                    echo 'Published';
                } else {
                    echo 'Unpublished';
                }
                ?>
            </td>
            <td>
                <a href="edit_blog.php?blog_id=<?php echo $blog_info['blog_id'];?>">Edit</a>||
                <a href="delete.php?blog_id=<?php echo $blog_info['blog_id'];?>">Delete</a>
            </td>
        </tr>
    <?php } ?>
</table>

